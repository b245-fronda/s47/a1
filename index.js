// console.log("JS DOM - Manipulation");

// [Section] Document Object Model(DOM)
	// Allows us to access or modify the properties of an html element in a webpage
	// it is standard on how to get, change, add or delete HTML elements
	// we will be focusing only with DOM in terms of managing forms

	// For selecting HTML elements we will be using document.querySelector / getElementById
		// Syntax: document.querySelector("html element")

		// CSS selectors
			// class selector (.)
			// id selector (#)
			// tag selector (html tags)
			// universal selector (*)
			// attribute selector ([attribute])

	// querySelectorAll
	let universalSelectorAll = document.querySelectorAll("*");
	console.log(universalSelectorAll);

	// querySelector
	let universalSelector = document.querySelector("*");
	console.log(universalSelector);

	let classSelector = document.querySelectorAll(".full-name");
	console.log(classSelector);

	let singleClassSelector = document.querySelector(".full-name");
	console.log(singleClassSelector);

	let tagSelector = document.querySelectorAll("input");
	console.log(tagSelector);

	let spanSelector = document.querySelector("span[id]");
	console.log(spanSelector);

	// getElement
		let element = document.getElementById("fullName");
		console.log(element);

		element = document.getElementsByClassName("full-name");
		console.log(element);

// [Section] Event Listeners
	// whenever a user interacts with a webpage, this action is considered as an event
	// working with events is large part of creating interactivity in a web page
	// specific function that will be triggered if the event happen.

	// The function used is "addEventListener", it takes two arguments
		// first argument is a string identifying the event
		// second argument is a function that the listener will trigger once the "specified event" occur.

		let txtFirstName = document.querySelector("#txt-first-name");

		// Add event listener
		txtFirstName.addEventListener("keyup", () => {
			spanSelector.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
		});

		let txtLastName = document.querySelector("#txt-last-name");

		txtLastName.addEventListener("keyup", () => {
			spanSelector.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
		});

		let textColor = document.querySelector("#text-color");

		textColor.addEventListener("change", () => {
			spanSelector.innerHTML = `<font color=${textColor.value}>${txtFirstName.value} ${txtLastName.value}</font>`
		})